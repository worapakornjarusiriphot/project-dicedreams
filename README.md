# Project DiceDreams

## Project Description
ในยุคสมัยที่สังคมมีการเชื่อมต่อผ่านอินเทอร์เน็ตและเทคโนโลยีสารสนเทศอย่างแพร่หลาย กิจกรรมทางสังคมเช่นการเล่นบอร์ดเกมก็ได้รับอิทธิพลจากการเปลี่ยนแปลงนี้ การเล่นบอร์ดเกมไม่เพียงแต่เป็นการพักผ่อนหย่อนใจ แต่ยังเป็นการสร้างความสัมพันธ์และการแลกเปลี่ยนวัฒนธรรมและความคิดเห็นระหว่างผู้คน ด้วยความนิยมที่เพิ่มขึ้นของบอร์ดเกม กลุ่มคนที่สนใจในบอร์ดเกมจึงมีจำนวนมากขึ้น แต่มักพบปัญหาในการหาเพื่อนหรือกลุ่มที่มีความสนใจเดียวกันเพื่อเล่นด้วยกัน

การมีระบบนัดหมายออนไลน์เพื่อเล่นบอร์ดเกมจึงมีความสำคัญในการตอบโจทย์ปัญหานี้ ระบบดังกล่าวช่วยให้ผู้ที่สนใจสามารถค้นหาและนัดหมายเพื่อเล่นบอร์ดเกมกับคนอื่นๆ ได้อย่างง่ายดาย นอกจากนี้ยังเป็นช่องทางในการแลกเปลี่ยนความคิดเห็นและประสบการณ์ที่เกี่ยวข้องกับบอร์ดเกม ซึ่งส่งผลในด้านบวกต่อการสร้างชุมชนและเครือข่ายทางสังคม

ปัญหาหลักที่ระบบนี้ต้องการแก้ไขคือการขาดแพลตฟอร์มที่เฉพาะเจาะจงสำหรับการพบปะและเล่นบอร์ดเกมแบบออนไซต์ รวมถึงการให้ข้อมูลที่เพียงพอเกี่ยวกับกลุ่มและกิจกรรมต่างๆ เกี่ยวกับบอร์ดเกม ความต้องการหลักคือการหาช่องทางในการเชื่อมต่อกลุ่มคนที่มีความสนใจเดียวกันและต้องการพบปะเพื่อเล่นบอร์ดเกมแบบออนไซต์
รายงานฉบับนี้จึงกล่าวถึงการสร้างระบบนัดหมายเพื่อเล่นบอร์ดเกมที่สามารถช่วยให้ผู้ที่สนใจหาเพื่อนที่มีความสนใจในกลุ่มเฉพาะและนัดกันเล่นได้ 

โปรเจคที่กำลังจะถูกสร้างขึ้นนี้มุ่งเน้นที่จะอำนวยความสะดวกให้กับบุคคลที่ต้องการเล่นแต่ไม่มีเพื่อนที่สนใจในบอร์ดเกมเช่นเดียวกันและยังช่วยสร้างโอกาสในการพบปะกับบุคคลใหม่ๆ สร้างสัมพันธภาพที่ดี ระบบนี้จึงมีความสำคัญอย่างยิ่งในการส่งเสริมการเชื่อมต่อและสร้างชุมชนที่แข็งแกร่งในหมู่ผู้ที่สนใจบอร์ดเกม


## Team Member
* นายวรปกร      จารุศิริพจน์
* นางสาวนวพร	บุญก่อน
* นายณัฐวุฒิ      แก้วมหา
* นายธีรณัฏฐ์      นุชเจริญ 

## Directory Structure
project/

│

├── Activity Diagram/ # โฟลเดอร์ Activity Diagram 

├── Class Diagram/ # โฟลเดอร์ Activity Diagram 

├── Document/ # โฟลเดอร์ Activity Diagram 

├── ER-Diagram/ # โฟลเดอร์ Activity Diagram 

├── Logo/ # โฟลเดอร์ Activity Diagram 

├── Presentstation/ # โฟลเดอร์ Activity Diagram 

├── Sequence Diagram/ # โฟลเดอร์ Activity Diagram 

├── UI(Figma)/ # โฟลเดอร์ Activity Diagram 

├── Use case diagram/ # โฟลเดอร์ Activity Diagram 

└── README.md


## ตำแหน่งงาน สัปดาห์ที่ 7
เว็บไซต์นัดกันเล่นบอร์ดเกม_16/2/2024
https://docs.google.com/document/d/1bcFIljMEtsR-e0P5yLdsnbLHGPamctuhCE7s28t7TYg/edit?usp=sharing

DiceDreams Use case diagram V.7
https://lucid.app/lucidchart/40888066-f0b7-4f51-aea5-2877e664d5ef/edit?invitationId=inv_2a38cfa7-4304-4ba2-a3e0-91b8171c9e3e&page=.-i.eZCmVCS3#
https://lucid.app/lucidchart/40888066-f0b7-4f51-aea5-2877e664d5ef/edit?invitationId=inv_2a38cfa7-4304-4ba2-a3e0-91b8171c9e3e&page=L-i.PDm72MwU#

DiceDreams ER-Diagram V.4
https://app.diagrams.net/#G1k4Fvd0Cj2ZKR0mboAcJdBL7r8BoVPhyV#%7B%22pageId%22%3A%22f7baGdrspGEwdL1HfaRs%22%7D

DiceDreams Activity Diagram V.3
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%22LnJOn1bJujt5n1gU2Dxs%22%7D
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%22ORIoPO-KC-yvLZJKLG0p%22%7D
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%22YFFXqpo50Vnv4IoCj3Ok%22%7D

DiceDreams Sequence Diagram V.1
https://app.diagrams.net/#G1dMGN_wiYYokH1FYj5jA-VRKyANaUBAn4#%7B%22pageId%22%3A%22a2WiT-7bPymSur5OZyeR%22%7D

DiceDreams UX/UI Figma V.4
https://www.figma.com/file/sEq4gNOKeUCPopg7UaKAi9/DiceDreams?type=design&node-id=0-1&mode=design&t=KvY24O7JlUST2Wle-0
https://www.figma.com/file/Houbf67bGxbqEJnCBxVUfV/Untitled?type=design&node-id=0-1&mode=design&t=eVKqTgjLLgsmqpAR-0
https://www.figma.com/file/KSc07HfT7HVGOImM3y45OY/Untitled?type=design&node-id=0%3A1&mode=design&t=LX7uCppQ2nCRojRm-1


## ตำแหน่งงาน สัปดาห์ที่ 6
เว็บไซต์นัดกันเล่นบอร์ดเกม_9/2/2024
https://docs.google.com/document/d/1x37hBBcbotkuejDq_0jMpqD93lp04j4AHCO8Jmees3E/edit?usp=sharing

DiceDreams Use case diagram V.6
https://lucid.app/lucidchart/40888066-f0b7-4f51-aea5-2877e664d5ef/edit?viewport_loc=-700%2C-281%2C6226%2C2747%2CXZ97JK9ceqNU&invitationId=inv_2a38cfa7-4304-4ba2-a3e0-91b8171c9e3e

DiceDreams ER-Diagram V.3
https://app.diagrams.net/#G1k4Fvd0Cj2ZKR0mboAcJdBL7r8BoVPhyV#%7B%22pageId%22%3A%22FUJS1yJywEuz17kTN7AN%22%7D

DiceDreams Activity Diagram V.3
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%22v6Pg-UlJo9AZlTwV6C6I%22%7D

DiceDreams Sequence Diagram V.1
https://app.diagrams.net/#G1dMGN_wiYYokH1FYj5jA-VRKyANaUBAn4#%7B%22pageId%22%3A%22a2WiT-7bPymSur5OZyeR%22%7D

DiceDreams UX/UI Figma V.3
https://www.figma.com/file/KSc07HfT7HVGOImM3y45OY/Untitled?type=design&node-id=0%3A1&mode=design&t=LX7uCppQ2nCRojRm-1


## ตำแหน่งงาน สัปดาห์ที่ 9
เอกสาร เว็บไซต์นัดกันเล่นบอร์ดเกม_1/3/2024
https://docs.google.com/document/d/1auh-1XO9ZSUnOH90cYwLcdzGB0H9UV-IL3-lG5HQ2gQ/edit?usp=sharing

DiceDreams Use case diagram V.8
https://lucid.app/lucidchart/40888066-f0b7-4f51-aea5-2877e664d5ef/edit?page=MQ5bNYG5CvSg&invitationId=inv_2a38cfa7-4304-4ba2-a3e0-91b8171c9e3e#
https://lucid.app/lucidchart/40888066-f0b7-4f51-aea5-2877e664d5ef/edit?invitationId=inv_2a38cfa7-4304-4ba2-a3e0-91b8171c9e3e&page=qQ5bqc~~KCJk#

DiceDreams ER-Diagram V.6
https://app.diagrams.net/#G1k4Fvd0Cj2ZKR0mboAcJdBL7r8BoVPhyV#%7B%22pageId%22%3A%22Br5YuvxrUDV1_5blCXQW%22%7D

DiceDreams Activity Diagram V.5
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%221bDZMVkSl_H96occn-cP%22%7D
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%22a52taMh5_oHudjuzdzN2%22%7D
https://app.diagrams.net/#G1OSgZPgDTdUkaQqzdkV2o9m1P9yRITigm#%7B%22pageId%22%3A%22PsvVeIfdwo9yIC4kD9q8%22%7D

DiceDreams Sequence Diagram V.1
https://app.diagrams.net/#G1dMGN_wiYYokH1FYj5jA-VRKyANaUBAn4#%7B%22pageId%22%3A%22a2WiT-7bPymSur5OZyeR%22%7D

DiceDreams UX/UI Figma V.6
https://www.figma.com/file/d61xAciGYwRBcjKbKf0bsP/DiceDreams-Group?type=design&node-id=0-1&mode=design&t=n9GQQjB6rqiL4gEf-0
https://www.figma.com/file/sEq4gNOKeUCPopg7UaKAi9/DiceDreams?type=design&node-id=0-1&mode=design&t=U5cTr23RDGQqhi0d-0
https://www.figma.com/file/KSc07HfT7HVGOImM3y45OY/finel-project?type=design&node-id=0-1&mode=design&t=pAa8G9IrDZ7HKgjm-0
https://www.figma.com/file/Houbf67bGxbqEJnCBxVUfV/Untitled?type=design&node-id=0-1&mode=design&t=Mpc6YHklgBOSKJeg-0